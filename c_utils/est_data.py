import os
from collections import Counter
import matplotlib.pyplot as plt

count_obj = []
count_image = 0
count_text = 0

path = r"E:\alcohol\labeled_data\all_data_al_aug"
name_label = ['lon bia', 'chai bia', 'ly bia', 'binh tru thap', 'binh tru cao',
              'binh olympia', 'binh vuong cao', 'binh vuong thap']

for fil in os.listdir(path):
    if fil.endswith('txt'):
        text_file = os.path.join(path, fil)
        with open(text_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                label = line.split(' ')[0]
                count_obj.append(name_label[int(label)])
        count_text += 1
    else:
        count_image += 1

print("Number of objects: ", len(count_obj))
print("Number of Images: ", count_image)
print("Number of text files, ", count_text)
dict_obj = Counter(count_obj)
print(dict_obj)

plt.figure(figsize=(15,7))
plt.bar(dict_obj.keys(), dict_obj.values())
plt.xlabel("Label")
plt.ylabel("Number object")
plt.show()
