import os
import cv2
import json
import torch
import numpy as np


def load_model(object_detect_weight, device):
    if device == 'cpu':
        yolo_obj_model = torch.hub.load('ultralytics/yolov5', 'custom', path=object_detect_weight,
                                        force_reload=True, device=device)
    else:
        yolo_obj_model = torch.hub.load('ultralytics/yolov5', 'custom', path=object_detect_weight, force_reload=True)
    return yolo_obj_model


def yolo_run(image, model, obj_conf=0.3, device="cpu"):
    list_box = []
    list_image = []
    list_label = []
    output = []
    model.conf = obj_conf
    model.to(device)
    model.names = ['bia', 'bia', 'bia', 'ruou', 'ruou', 'ruou', 'ruou', 'ruou']
    results = model(cv2.cvtColor(image, cv2.COLOR_BGR2RGB), size=640)  # includes NMS
    # results = model(cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB), size=640)  # includes NMS
    # results = model(image, size=640)
    label_files = results.pred[0].tolist()
    names = results.names
    for num, file in enumerate(label_files):
        boxes = file[:4]
        label = file[-1]
        ymin = round((float(boxes[0])))
        xmin = round((float(boxes[1])))
        ymax = round((float(boxes[2])))
        xmax = round((float(boxes[3])))
        iloc = [xmin, ymin, xmax, ymax]

        cropped_image = image[xmin:xmax, ymin:ymax].copy()
        list_box.append(iloc)
        list_image.append(cropped_image)
        list_label.append(names[int(label)])
        output = [list_box, list_image, list_label]
    return output


def draw_box(list_box, list_image, list_label, image_path=None, input_image=None):
    if input_image is not None:
        image = input_image
    else:
        image = cv2.imread(image_path)
    for num, img in enumerate(list_image):
        if list_box[num] != 'None':
            xmin, ymin, xmax, ymax = list_box[num]
            image = cv2.rectangle(image, (ymin, xmin), (ymax, xmax), (36, 255, 12), 1)
            label = list_label[num]
            cv2.putText(image, str(label), (ymin, xmin - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36, 255, 12), 2)
    return image


def save_obj(save_path, image_path, image):
    if save_path is None:
        name = image_path + "_pred.jpg"
        cv2.imwrite(name, image)
        print(name)
    else:
        name = os.path.join(save_path, os.path.basename(image_path))
        cv2.imwrite(name, image)
        print(name)
    print("-------------------")


def object_detect(input, yolo_obj_model, obj_conf=0.33, device="cpu"):
    if type(input) == str:
        image = cv2.imread(input)
    image = input
    yolo_output = yolo_run(image, yolo_obj_model, obj_conf=obj_conf, device=device)
    if len(yolo_output) != 0:
        list_box, list_image, list_label = yolo_output
    else:
        list_box = ["None"]
        list_image = ["None"]
        list_label = ["None"]
    return list_box, list_image, list_label


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        if isinstance(obj, np.int32):
            return int(obj)
        if isinstance(obj, np.int64):
            return int(obj)
        if isinstance(obj, np.float32):
            return float(obj)
        if isinstance(obj, np.float64):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

