# Alcohol detection
## Installation

#### 1. Clone repository:
```
git clone https://gitlab.com/toannguyen1900/alcohol.git
```
#### 2. Install dependencies:
```
pip install -r requirements.txt
```
## Inference model
```
python API.py --weight [YOUR_WEIGHT_PATH] --conf_thres [exp: 0.2] --device [cpu or cuda]
```
#### Arguments:
* `--weight`: pretrained weight path                    -- default: './weight/alcohol_weight.pt'
* `--conf_thres`: Confidence threshold for yolo model   -- default: 0.2
* `--device`: cpu or gpu if you have cuda               -- default: cpu
## Return format
```
{ 
    "Device": "cuda" or "cpu",
    "Image_0": {
        "Image_0_url": "Image link",
        "Object_0": "Object label"
    },
    "Success": true (if there is an image)
}
```
***
