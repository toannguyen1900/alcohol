import io
import os
import cv2
import torch
import argparse
import numpy as np
from PIL import Image
from datetime import datetime
from flask import Flask, request, jsonify
from c_utils.utils import load_model, object_detect, draw_box

app = Flask(__name__, static_url_path='/static')
global yolo_model, device, ts

yolo_model = None
dt = datetime.now()
ts = datetime.timestamp(dt)
parser = argparse.ArgumentParser()

parser.add_argument('--save_path', type=str, default=r'.\static\images')
parser.add_argument('--weight', type=str, default=r'.\weight\alcohol_weight.pt')
parser.add_argument('--device', type=str, default='cpu' )
# device = torch.device("cpu") if not torch.cuda.is_available() else torch.device("cuda")

@app.route('/')
def test_model():
    return "This is a test"


@app.route('/predict', methods=['POST'])
def predict():
    data = {"Success": False}
    data["Device"] = str(device)
    print(device)
    try:
        if request.files.getlist("Image"):
            images = request.files.getlist("Image")
            for num, image in enumerate(images):
                data[f"Image_{num}"] = {}
                try:
                    image = image.read()
                    if len(image) == 0:
                        data[f"Image_{num}"] = "No image file input"
                        continue
                    image = Image.open(io.BytesIO(image))
                    image = np.array(image)
                    list_box, list_image, list_label = object_detect(input=image, yolo_obj_model=yolo_model, device=device)
                    result_image = draw_box(list_box, list_image, list_label, input_image=image)
                    save_path = os.path.join(args.save_path, f"image_{ts}{num}.jpg")
                    cv2.imwrite(save_path, cv2.cvtColor(result_image, cv2.COLOR_BGR2RGB))

                    link = "http://192.168.1.53:5000/static/images" + "/" + f"image_{ts}{num}.jpg"
                    url = {f"Image_{num}_url": link}

                    for num_obj, label in enumerate(list_label):
                        obj = {f"Object_{num_obj}": label}
                        data[f"Image_{num}"].update(obj)
                    data[f"Image_{num}"].update(url)
                    data["Success"] = True

                except:
                    data[f"Image_{num}"] = "Not an image file"
    except Exception as e:
        print(e)
    return jsonify(data)


if __name__ == '__main__':
    print("App run!!!")
    args = parser.parse_args()
    if args.device == 'cpu':
        device = torch.device("cpu")
    else:
        device = torch.device("cuda")
    yolo_model = load_model(args.weight, device)
    app.run(host='0.0.0.0', debug=True)